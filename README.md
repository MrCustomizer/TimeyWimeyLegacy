# TimeyWimey
This is the software component for a Raspberry Pi-based (Internet radio) alarm clock written in C++/Qt. It's using a PiTFT as display and main input system. Optionally, you can attach a light sensor to dim the display automatically, if the surroundings are getting dark.

## Status
This project started as a proof of concept and ended up as the alarm clock in my bedroom. The plan is to stabilize this project and add some more features (for example switching to another radio stream). For more details on my current plans take a look at the [projects page](https://github.com/MrCustomizer/TimeyWimey/projects).

## Prerequisites

### Hardware
The following hardware components are the parts I use for testing and developing. The project could probably adapted
be adapted to work with other Raspberry Pi models and/or displays but my focus is currently on getting it to
work on these components and adding features more than supporting a broader base of hardware.

#### Necessary hardware
* A Raspberry Pi 3
* A PiTFT touchscreen. I have tested it with the capacitative PiTFT 2.8". Other versions or other touchscreens probably work too, although minor adjustments could be necessary.


#### Optional hardware
* A light sensor of the type TSL2561 for automatic brightness adjustment of the display and a separate USB soundcard.

If you want to use the backlight dimming of your display, you need a separate USB sound card as the internal soundcard of the Pi uses PWM-modules which are needed fog the dimming. So you can only use either the backlight dimming or the internal sound card at the same time. 

## Setup
### Assembly of the hardware
I will probably add some manual on how to put the hardware components together in the future. If you have questions, you can contact me through the bug tracker of this project or by joining the chat room of the project:
[#timeywimey:localtoast.matrix.de](https://matrix.to/#/#timeywimey:matrix.localtoast.de)

#### Software
I recommend running TimeyWimey on Raspbian Stretch Lite. No desktop environment has to be present to run this application. The following guide assumes, that you are running Raspbian Stretch Lite.

Before starting with the setup, make sure that the system is up to date:
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```


#### 1. Setting up your PiTFT-display
The display has to be configured without rotation. If you are using Raspbian Stretch, the current way to get your PiTFT working, is this (They are planning to include the script in the official PiTFT-documentation. [Source 1](https://forums.adafruit.com/viewtopic.php?f=47&t=122926&p=629573#p629573) and [source 2](https://github.com/adafruit/Adafruit-PiTFT-Helper/issues/19)):

```
cd ~
wget https://raw.githubusercontent.com/adafruit/Adafruit-PiTFT-Helper/master/adafruit-pitft-helper2.sh -O adafruit-pitft-helper2.sh
chmod +x adafruit-pitft-helper2.sh
sudo ./adafruit-pitft-helper2.sh
```

The script starts a wizard. If you have the capacitive version of the PiTFT (for which this software is optimized and on which it is tested), use the following settings:
```
3 (PiTFT 2.8" capacitive touch (240x320))
3 (270 degrees (portait))
n (Would you like the console to appear on the PiTFT display? [y/n])
y (Would you like the HDMI display to mirror to the PiTFT display? [y/n])

```


You have to correct the rotation of the display by using the parameter `display_rotate`. This
is important as otherwise it is possible, that the touch screen and the display rotation don't line up. It should work if the last lines of your  `/boot/config.txt` looks similar to this:

```
# --- added by adafruit-pitft-helper Mon  5 Feb 20:09:19 UTC 2018 ---
[pi0]
device_tree=bcm2708-rpi-0-w.dtb
[pi1]
device_tree=bcm2708-rpi-b-plus.dtb
[pi2]
device_tree=bcm2709-rpi-2-b.dtb
[pi2]
device_tree=bcm2710-rpi-3-b.dtb
[all]
dtparam=spi=on
dtparam=i2c1=on
dtparam=i2c_arm=on
dtoverlay=pitft28-capacitive,rotate=270,speed=64000000,fps=30
# --- end adafruit-pitft-helper Mon  5 Feb 20:09:19 UTC 2018 ---
hdmi_cvt=320 240 60 1 0 0 0
display_rotate=1


```

#### 2. Dimming the PiTFT with a light sensor (Optional)
If you want to use the light sensor you have to have a separate soundcard as the PWM module used by the internal soundcard is needed for the dimming of the PiTFT's backlight. Using the PWM module for dimming means it is blocked and the internal soundcard cannot be used any more.

##### Setting up the USB sound card

Install and setup pulseaudio, so that it is using the USB soundcard by default instead of the internal one:
```
sudo apt-get update
sudo apt-get install pulseaudio
pulseaudio -D
```
Search for the index of your USB soundcard by reading the output of `pacmd list-sinks`. Then set
this card's index as default with `pacmd set-default-sink <index>`.


##### Enable the I2C interface (for the light sensor)
Start raspi-config:
```
sudo raspi-config
```
Then enter menu point `5 Interfacing Options` and select `P5 I2C` and enable it. 

#### 3. Install wiringpi
Follow [this guide](http://wiringpi.com/download-and-install/) to install wiringpi.


#### 4. Set correct timezone
You should set your current timezone. Do that by using raspi-config:
```
sudo raspi-config
```
Switch to `4 Localisation Options`, then enter `I2 Change Timezone`, then select your timezone.

#### 5. Install dependencies
You need to install some dependencies, before being able to run TimeyWimey. If you want to cross compile TimeyWimey, stop here and continue in the section [Compile TimeyWimey](##Compile-TimeyWimey). Otherwise execute the following commands:

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install pulseaudio qt5-default libqt5quickcontrols2-5
```

After the next reboot, your Pi should be ready to run TimeyWimey!


## Compile TimeyWimey
### Setup cross compile toolchain
Follow this guide:
https://wiki.qt.io/RaspberryPi2EGLFS

**Attention:** If you have already setup your PiTFT, step 4 (`sudo rpi-update`) could reset your PiTFT setup, so that you have to do it again. At least on my Pi 3 with Raspbian Stretch, this step was not necessary, so I left it out. It's possible, this step is referring to an older version or Raspian Stretch.



## Run TimeyWimey
To run TimeyWimey, simply copy the binary to the the pi and run it from the shell.

### Autostart
The easiest way to autostart TimeyWimey is probably by using crontab with the `@reboot`-parameter. The pulseaudio-Daemon has be started TimeyWimey. I suggest the following setup:
* Create a simple startup script with the name `startTimeyWimey.sh` in your home directory, which executes pulseaudio and TimeyWimey:
```
#!/bin/bash
pulseaudio -D
/home/pi/TimeyWimey
```
* Put this script in the user's crontab (run `crontab -e` to edit the user's crontab file):
```
# m h  dom mon dow   command
@reboot /home/pi/startTimeyWimey.sh
```

That's it. After the next reboot, TimeyWimey should start automatically.

## Contact
Matrix:
- Join [#timeywimey:localtoast.matrix.de](https://matrix.to/#/#timeywimey:matrix.localtoast.de)
