#-------------------------------------------------
#
# Project created by QtCreator 2015-11-18T19:07:04
#
#-------------------------------------------------

INSTALL_PATH = "/home/pi"

TEMPLATE = app
QT += qml quick multimedia network widgets
CONFIG += c++11
TARGET = TimeyWimey
LIBS += -lwiringPi

RESOURCES += resources.qrc

DISTFILES += timeywimey.astylerc

INCLUDEPATH += $$[QT_SYSROOT]/usr/local/include

SOURCES += \
    libs/tsl2561/src/tsl2561.c \
    src/alarms/alarm.cpp \
    src/alarms/alarmeditor.cpp \
    src/alarms/alarmlistmodel.cpp \
    src/alarms/alarmmanager.cpp \
    src/mediaplayer/mediaplayer.cpp \
    src/mediaplayer/volumecontrol.cpp \
    src/sensors/backlightdimmer.cpp \
    src/sensors/lightsensortracker.cpp \
    src/sensors/pitftbuttonhandler.cpp \
    src/sensors/wiringpitrigger.cpp \
    src/main.cpp \
    src/settingsmanager.cpp \
    src/systemhelpers.cpp


HEADERS  += \
    libs/tsl2561/src/i2c-dev.h \
    libs/tsl2561/src/tsl2561.h \
    src/alarms/alarm.h \
    src/alarms/alarmeditor.h \
    src/alarms/alarmlistmodel.h \
    src/alarms/alarmmanager.h \
    src/mediaplayer/mediaplayer.h \
    src/mediaplayer/volumecontrol.h \
    src/sensors/backlightdimmer.h \
    src/sensors/lightsensortracker.h \
    src/sensors/pitftbuttonhandler.h \
    src/sensors/wiringpitrigger.h \
    src/settingsmanager.h \
    src/systemhelpers.h


# copy executable to pi dir
target.path = $$INSTALL_PATH
INSTALLS += target
