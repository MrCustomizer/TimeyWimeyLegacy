import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

Rectangle {
    id: bottomPanel
    width: parent.width
    height: view.smallBorderSize
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    color: "#00FFFFFF"
    
        
    Text {
        id: nextAlarm
        text: getFormattedText(AlarmManager.getNextAlarmString(),
                               AlarmManager.getTimeToNextAlarmString())
        font.bold: true
        font.family: robotoBold
        textFormat: Text.RichText
        anchors.centerIn: parent

        Timer {
            interval: 1000
            repeat: true
            running: true

            onTriggered: parent.updateText()
        }

        Component.onCompleted: Settings.onAlarmChanged.connect(updateText)

        function getFormattedText(nextAlarm, timeToNextAlarm) {
            var start = "<font style=\"font-size: 9px; color:" +
                    view.grey + "\">next alarm</font>";
            var startBlue = "<font style=\"font-size: 17px; color:" + view.yellow + "\"> ";
            var end = "</font>";
            if (nextAlarm === "") {
                return startBlue + "no alarms set" + end;
            }

            return start +  startBlue + timeToNextAlarm + " (" + nextAlarm + ")" + end;
        }

        function updateText() {
            nextAlarm.text = nextAlarm.getFormattedText(AlarmManager.getNextAlarmString(),
                                              AlarmManager.getTimeToNextAlarmString())
        }
    }
}
