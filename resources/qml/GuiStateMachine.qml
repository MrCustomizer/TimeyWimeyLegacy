import QtQuick 2.0
import QtQml.StateMachine 1.0 as DSM

DSM.StateMachine {
    running: true
    childMode: DSM.QState.ParallelStates

    signal enterHome
    signal enterAlarms
    signal enterSlumber

    DSM.State {
        id: screenStates
        initialState: defaultScreen

        DSM.State {
            id: defaultScreen
            initialState: homeSectionState

            DSM.State {
                id: homeSectionState
                initialState: defaultHomeState

                DSM.State {
                    id: defaultHomeState

                    onEntered: {
                        screens.homeScreen.state = "active"
                        screens.blurTint.state = "default"
                        view.state = "bigBorders"
                    }
                    onExited: {
                        screens.homeScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: bufferingHomeState
                        signal: screens.homeScreen.mouseArea.clicked
                        onTriggered: MediaPlayer.start()
                    }
                }

                DSM.State {
                    id: bufferingHomeState
                    onEntered: {
                        screens.bufferingScreen.state = "active"
                        view.state = "bigBorders"
                        if (MediaPlayer.isPlaying())
                        {
                            MediaPlayer.finishedBuffering()
                        }
                    }
                    onExited: {
                        screens.bufferingScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: playingHomeState
                        signal: MediaPlayer.finishedBuffering
                    }
                 }

                DSM.State {
                    id: playingHomeState

                    onEntered: {
                        screens.playingScreen.state = "active"
                        view.state = "smallBorders"
                    }
                    onExited: {
                        screens.playingScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: defaultHomeState
                        signal: screens.playingScreen.mouseArea.clicked
                        onTriggered: MediaPlayer.stop()
                    }
                }

                DSM.HistoryState {
                    id: homeHistoryState
                    defaultState: defaultHomeState
                }


                DSM.SignalTransition {
                    targetState: activeAlarmState
                    signal: AlarmManager.alarmTriggered
                }
                DSM.SignalTransition {
                    onTriggered: navigationStates.enterNavi()
                    signal: topPanel.mouseArea.clicked
                }
                DSM.SignalTransition {
                    targetState: alarmsSectionHistoryState
                    signal: enterAlarms
                }
                DSM.SignalTransition {
                    targetState: slumberBufferingState
                    signal: enterSlumber
                }
            }


            DSM.State {
                id: alarmsSectionState
                initialState: alarmsSectionHistoryState

                onEntered: {
                    screens.blurTint.state = "default"
                }

                DSM.State {
                    id: alarmListState
                    onEntered: {
                        screens.alarmsScreen.state = "active"
                        view.state = "smallBorders"
                    }
                    onExited: {
                        screens.alarmsScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: editAlarmHoursState
                        signal: screens.alarmsScreen.newAlarmMouseArea.clicked
                        onTriggered: {
                            AlarmEditor.createNewAlarm()
                            screens.alarmEditorScreen.refresh()
                        }
                    }

                    DSM.SignalTransition {
                        targetState: editAlarmHoursState
                        signal: screens.alarmsScreen.editAlarm
                        onTriggered: {
                            AlarmEditor.startEditing(screens.alarmEditorScreen.editAlarmIndex)
                            screens.alarmEditorScreen.refresh()
                        }
                    }
                }

                DSM.State {
                    id: alarmEditorState
                    initialState: editAlarmHistoryState

                    onEntered: {
                        screens.alarmEditorScreen.state = "active"
                        view.state = "smallBorders"
                    }
                    onExited: {
                        screens.alarmEditorScreen.state = "inactive"
                    }

                    DSM.State {
                        id: editAlarmMinutesState
                        onEntered: {
                            screens.alarmEditorScreen.switchToMinutes()
                        }

                        DSM.SignalTransition {
                            targetState: editAlarmHoursState
                            signal: screens.alarmEditorScreen.hourMouseArea.clicked
                        }
                    }
                    DSM.State {
                        id: editAlarmHoursState
                        onEntered: {
                            screens.alarmEditorScreen.switchToHours()
                        }

                        DSM.SignalTransition {
                            targetState: editAlarmMinutesState
                            signal: screens.alarmEditorScreen.minutesMouseArea.clicked
                        }
                    }

                    DSM.HistoryState {
                        id: editAlarmHistoryState
                        defaultState: editAlarmHoursState
                    }

                    DSM.SignalTransition {
                        targetState: alarmListState
                        signal: screens.alarmEditorScreen.okButtonMouseArea.clicked
                        onTriggered: {
                            AlarmEditor.confirmAlarm()
                            screens.alarmEditorScreen.switchToHours()
                        }
                    }
                    DSM.SignalTransition {
                        targetState: alarmListState
                        signal: screens.alarmEditorScreen.cancelButtonMouseArea.clicked
                        onTriggered: {
                            AlarmEditor.abortAlarmEditing()
                            screens.alarmEditorScreen.switchToHours()
                        }
                    }

                }

                DSM.HistoryState {
                    id: alarmsSectionHistoryState
                    defaultState: alarmListState
                }

                DSM.SignalTransition {
                    targetState: activeAlarmState
                    signal: AlarmManager.alarmTriggered
                }
                DSM.SignalTransition {
                    onTriggered: navigationStates.enterNavi()
                    signal: topPanel.mouseArea.clicked
                }
                DSM.SignalTransition {
                    targetState: homeHistoryState
                    signal: enterHome
                }
                DSM.SignalTransition {
                    targetState: slumberBufferingState
                    signal: enterSlumber
                }
            }

            DSM.State {
                id: slumberState
                initialState: slumberBufferingState

                DSM.State {
                    id: slumberBufferingState
                    onEntered: {
                        screens.bufferingScreen.state = "active"
                        view.state = "bigBorders"
                        if (MediaPlayer.isPlaying())
                        {
                            MediaPlayer.finishedBuffering()
                        }
                    }
                    onExited: {
                        screens.bufferingScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: slumberPlayingState
                        signal: MediaPlayer.finishedBuffering
                    }
                }

                DSM.State {
                    id: slumberPlayingState

                    onEntered: {
                        screens.slumberScreen.state = "active"
                        screens.blurTint.state = "slumberAndSnooze"
                        view.state = "smallBorders"
                    }
                    onExited: {
                        screens.slumberScreen.state = "inactive"
                    }

                    DSM.SignalTransition {
                        targetState: defaultHomeState
                        signal: screens.slumberScreen.mouseArea.clicked
                        onTriggered: MediaPlayer.stop()
                    }
                }

                DSM.SignalTransition {
                    targetState: defaultHomeState
                    signal: MediaPlayer.stopped
                }
                DSM.SignalTransition {
                    targetState: activeAlarmState
                    signal: AlarmManager.alarmTriggered
                }
                DSM.SignalTransition {
                    onTriggered: navigationStates.enterNavi()
                    signal: topPanel.mouseArea.clicked
                }
                DSM.SignalTransition {
                    targetState: playingHomeState
                    signal: enterHome
                }
                DSM.SignalTransition {
                    targetState: alarmsSectionHistoryState
                    signal: enterAlarms
                }
            }
        }

        DSM.State {
            id: activeAlarmState
            initialState: activeAlarmBufferingState

            DSM.State {
                id: activeAlarmBufferingState
                onEntered: {
                    screens.blurTint.state = "alarm"
                    screens.bufferingScreen.state = "active"
                    view.state = "smallBorders"
                    if (MediaPlayer.isPlaying())
                    {
                        MediaPlayer.finishedBuffering()
                    }
                }
                onExited: {
                    screens.bufferingScreen.state = "inactive"
                }

                DSM.SignalTransition {
                    targetState: activeAlarmPlayingState
                    signal: MediaPlayer.finishedBuffering
                }
            }

            DSM.State {
                id: activeAlarmPlayingState
                onEntered: {
                    screens.blurTint.state = "alarm"
                    screens.alarmPlayingScreen.state = "active"
                    view.state = "smallBorders"
                }
                onExited: {
                    screens.alarmPlayingScreen.state = "inactive"
                }

                DSM.SignalTransition {
                    targetState: snoozeState
                    signal: screens.alarmPlayingScreen.mouseArea.clicked
                    onTriggered: AlarmManager.snooze()
                }
            }
            DSM.State {
                id: snoozeState
                onEntered: {
                    screens.blurTint.state = "slumberAndSnooze"
                    screens.snoozeScreen.state = "active"
                    view.state = "smallBorders"
                }
                onExited: {
                    screens.snoozeScreen.state = "inactive"
                    AlarmManager.stopSnoozing()
                }

                DSM.SignalTransition {
                    targetState: defaultHomeState
                    signal: screens.snoozeScreen.mouseArea.clicked
                }
                DSM.SignalTransition {
                    targetState: activeAlarmBufferingState
                    signal: AlarmManager.alarmTriggered
                }
            }

            DSM.SignalTransition {
                signal: topPanel.mouseArea.clicked
                onTriggered: navigationStates.enterNavi()
            }

            DSM.SignalTransition {
                targetState: homeHistoryState
                signal: enterHome
            }
            DSM.SignalTransition {
                targetState: alarmsSectionHistoryState
                signal: enterAlarms
            }
            DSM.SignalTransition {
                targetState: slumberBufferingState
                signal: enterSlumber
            }
        }
    }

    DSM.State {
        id: navigationStates
        initialState: naviInactive

        signal enterNavi
        signal piTftButtonPressed

        Component.onCompleted: {
            if (SystemHelpers.isRunningOnPi()) {
                PiTftButtonHandler.buttonPressed.connect(piTftButtonPressed)
            }
        }

        DSM.State {
            id: naviInactive

            DSM.SignalTransition {
                targetState: naviActive
                signal: navigationStates.enterNavi
            }
            DSM.SignalTransition {
                targetState: naviActive
                signal: navigationStates.piTftButtonPressed
            }
        }

        DSM.State {
            id: naviActive

            onEntered: {
                navigationPanel.state = "active"
            }
            onExited: {
                navigationPanel.state = "inactive"
            }

            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationPanel.mouseAreaHome.clicked
                onTriggered: enterHome()
            }
            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationPanel.mouseAreaAlarmEditor.clicked
                onTriggered: enterAlarms()
            }
            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationPanel.mouseAreaSlumber.clicked
                onTriggered: {
                    enterSlumber()
                    MediaPlayer.startSlumberMode()
                }
            }

            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationStates.exitNavi
            }
            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationStates.piTftButtonPressed
            }
            DSM.SignalTransition {
                targetState: naviInactive
                signal: navigationPanel.exitNavi
            }

        }
    }
}
