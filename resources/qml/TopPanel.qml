import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

import "qrc:/qml/elements"

Item {
    // TODO only add app title, mousearea and clock
    id: topPanel
    width: 320
    anchors.left: parent.left
    anchors.leftMargin: 0
    anchors.top: parent.top

    property alias mouseArea: mouseArea

    Text {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 10
        textFormat: Text.RichText
        font.family: robotoBold
        font.pixelSize: 12
        font.bold: true
        text: "<font style=\"color:" + view.yellow +
              "\">Timey</font><font style=\"color:white\">Wimey</font>"
    }
       
    Text {
        id: topClock
        color: "#ffffff"
        text: Qt.formatTime(new Date(), "hh:mm")
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 8
        font.pixelSize: 10
        font.bold: true
        font.family: robotoBold
        Timer {
            interval: 1000
            repeat: true
            running: true
            
            onTriggered: {
                topClock.text = Qt.formatTime(new Date(), "hh:mm")
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }
}
