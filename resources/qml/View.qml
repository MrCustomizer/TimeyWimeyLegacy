import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

import "qrc:/qml/elements"
import "qrc:/qml/screens"

Rectangle {
    id: view
    color: "#00ffffff"
    z: 0
    border.width: 0
    width: 320
    height: 240
    anchors.top: parent.top
    anchors.left: parent.left
    transformOrigin: Item.Center
    rotation: 0
    opacity: 1
    
    property string yellow: "#c4be0c"
    property string grey: "#b3b3b3"

    property int smallBorderSize: 25
    property int bigBorderSize: 60
    
    GuiStateMachine {
    }
    
    Wallpaper {
        id: wallpaper
    }
    
    Rectangle {
        id: wallpaperTint
        anchors.fill: parent
        color: "#88000000"
    }
    
    BrightnessModifier {
        id: backgroundBrightnessModifier
    }
    
    Screens {
        id: screens
    }

    TopPanel {
        id: topPanel
    }
       
    BottomPanel {
        id: bottomPanel
    }

    Navigation {
        id: navigationPanel
    }
    
    Component.onCompleted: {
        LightSensorTracker.sensorUpdated.connect(lightSensorUpdated)
    }

    function lightSensorUpdated(lux) {
        setBackgroundAlpha(lux)
    }

    function setBackgroundAlpha(lux) {
        var newAlpha = 1 - (lux * 7 / 100);

        var max = 1;
        var min = 0.4;
        if (newAlpha > max) {
            newAlpha = max;
        } else if (newAlpha < min) {
            newAlpha = min;
        }

        screens.blurBrightnessModifierColor.a = newAlpha;
        backgroundBrightnessModifier.color.a = newAlpha;
    }

    states: [
        State {
            name: "smallBorders"
            PropertyChanges {
                target: screens
                height: 240 - 2 * view.smallBorderSize
            }
            PropertyChanges {
                target: screens
                anchors.topMargin: view.smallBorderSize
            }
            PropertyChanges {
                target: topPanel
                height: view.smallBorderSize
            }
            PropertyChanges {
                target: bottomPanel
                height: view.smallBorderSize
            }
        },
        State {
            name: "bigBorders"
            PropertyChanges {
                target: screens
                height: 240 - 2 * view.bigBorderSize
            }
            PropertyChanges {
                target: screens
                anchors.topMargin: view.bigBorderSize
            }
            PropertyChanges {
                target: topPanel
                height: view.bigBorderSize
            }
            PropertyChanges {
                target: bottomPanel
                height: view.bigBorderSize
            }
        }
    ]
    transitions: Transition {
        PropertyAnimation {
            properties: "height,anchors.topMargin"
            easing.type: "OutQuart"
            duration: animDuration
        }
    }
}
