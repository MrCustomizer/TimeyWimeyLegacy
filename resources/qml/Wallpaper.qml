import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

Image {
    id: wallpaper
    width: parent.width
    height: parent.height
    anchors.top: parent.top
    anchors.topMargin: 0
    anchors.left: parent.left
    anchors.leftMargin: 0
    source: "qrc:/graphics/back.png"
}
