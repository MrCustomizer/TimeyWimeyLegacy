import QtQuick 2.0

Rectangle {
    id: artistAndTitleBanner
    clip: true
    width: 270
    height: 18
    anchors.horizontalCenter: parent.horizontalCenter
    color: "#00000000"

    MarqueeItem {
        id: marqueeItem
        width: content.width

        Item {
            id: content
            width: artistLabel.width + artistMarqueeText.anchors.leftMargin +
                   artistMarqueeText.width + titleLabel.anchors.leftMargin + titleLabel.width +
                   titleMarqueeText.anchors.leftMargin + titleMarqueeText.width
            height: parent.height

            Text {
                id: artistLabel
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                text: "artist"

                color: grey
                font.family: robotoBold
                font.bold: true
                font.pixelSize: 9
            }
            Text {
                id: artistMarqueeText
                anchors.left: artistLabel.right
                anchors.leftMargin: 3
                anchors.bottom: parent.bottom

                color: yellow
                font.family: roboto
                font.pixelSize: 18
                font.bold: true
            }
            Text {
                id: titleLabel
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.left: artistMarqueeText.right
                anchors.leftMargin: 20

                text: "title"

                color: grey
                font.family: robotoBold
                font.bold: true
                font.pixelSize: 9
            }
            Text {
                id: titleMarqueeText
                anchors.left: titleLabel.right
                anchors.leftMargin: 3
                anchors.bottom: parent.bottom

                color: yellow
                font.family: roboto
                font.pixelSize: 18
                font.bold: true
            }
        }

        function artistTextChanged(newText) {
            var oldText = artistMarqueeText.text
            if (oldText !== newText) {
                artistMarqueeText.text = newText
                widthChanged()
            }
        }
        function titleTextChanged(newText) {
            var oldText = titleMarqueeText.text
            if (oldText !== newText) {
                titleMarqueeText.text = newText
                widthChanged()
            }
        }

        Component.onCompleted: {
            MediaPlayer.newArtist.connect(artistTextChanged)
            MediaPlayer.newTitle.connect(titleTextChanged)
        }
    }

}
