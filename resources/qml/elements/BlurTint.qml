import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

Rectangle {
    id: blurTint
    anchors.fill: blur
    
    state: "default"
    
    states: [
        State {
            name: "default"
            PropertyChanges {
                target: blurTint
                color: "#00940000"
            }
        },
        State{
            name: "alarm"
            PropertyChanges {
                target: blurTint
                color: "#44940000"
            }
        },
        State{
            name: "slumberAndSnooze"
            PropertyChanges {
                target: blurTint
                color: "#44283cb7"
            }
        }
    ]
    transitions: [
        Transition {
            from: "*"
            to: "*"
            SequentialAnimation {
                ColorAnimation {
                    target: blurTint
                    easing {
                        type: Easing.OutQuart
                    }
                    duration: animDuration
                }
            }
        }
    ]
}
