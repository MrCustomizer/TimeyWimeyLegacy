import QtQuick 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

import "qrc:/qml/elements"

Item {
    id: circleSelector
    width: 100
    height: width
    property int mouseAreaRingWidth: 30
    property int from: 0
    property int to: 100
    property int leftMargin: 0
    signal valueChanged

    state: "active"
    
    ProgressBar {
        id: circleProgressBar
        anchors.centerIn: parent
        
        width: circleSelector.width - (circleSelector.mouseAreaRingWidth)
        height: width
        value: 0
        
        style: ProgressBarStyle
        {
            panel : Rectangle
            {
                color: "transparent"
                width: circleProgressBar.width
                height: width
                
                Rectangle
                {
                    id: ring
                    z: 0
                    anchors.centerIn: parent
                    width: parent.width - 2
                    height: parent.height - 2
                    radius: Math.max(width, height) / 2
                    color: "transparent"
                    border.color: "black"
                    border.width: 4

                    opacity: 0
                }

                ConicalGradient
                {
                    id: filledRingPart
                    source: ring
                    anchors.fill: parent
                    property string fillColor: "#BB7deeff"

                    gradient: Gradient
                    {
                        GradientStop { position: 0.00; color: filledRingPart.fillColor}
                        GradientStop { position: control.value; color: filledRingPart.fillColor }
                        GradientStop { position: control.value + 0.01; color: "transparent" }
                        GradientStop { position: 1.00; color: "transparent" }
                    }
                }

                ConicalGradient
                {
                    id: emptyRingPart
                    source: ring
                    anchors.fill: parent

                    property string emptyColor: "#99999999"

                    gradient: Gradient
                    {
                        GradientStop { position: 0.00; color: "transparent" }
                        GradientStop { position: control.value; color: "transparent" }
                        GradientStop { position: control.value + 0.01; color: emptyRingPart.emptyColor }
                        GradientStop { position: 1.00; color: emptyRingPart.emptyColor }
                    }
                }
                
                Text
                {
                    id: progressLabel
                    anchors.centerIn: parent
                    color: "white"
                    font.family: base.robotoBold
                    font.pixelSize: 20
                    text: circleSelector.calcExternalValueFromInternalValue()
                }
            }
        }

        onValueChanged:
        {
            circleSelector.valueChanged()
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        
        onPositionChanged: {
            if (mouse.buttons === 1) {
                setValueFromMousePosition(mouse.x, mouse.y)
            }
        }
        
        onPressed: {
            setValueFromMousePosition(mouse.x, mouse.y)
        }

        function setValueFromMousePosition(x, y) {
            // normalize to coordinate system, which centers in the widget's center
            x = x - (circleSelector.width / 2)
            y = y - (circleSelector.width / 2)
            var positionRadius = Math.sqrt(x * x + y * y)

            var outerRadius = circleSelector.width / 2
            var innerRadius = 5
            if (innerRadius < positionRadius && outerRadius > positionRadius)
            {
                circleProgressBar.value = calculateValue(x, y)
            }
        }
        
        function calculateValue(x, y) {
            var angleRadians = Math.atan2(y, x)
            var angleDegree = 0
            if (angleRadians > 0) {
                angleDegree = angleRadians * 360 / (2 * Math.PI)
            }else {
                angleDegree = (2 * Math.PI + angleRadians) * 360 / (2 * Math.PI)
            }
            
            var value = angleDegree / 360 + 0.25
            if (value >= 1)
            {
                value = value - 1
            }
            
            return value
        }
    }
    
    function calcExternalValueFromInternalValue() {
        var to = circleSelector.to
        var from = circleSelector.from
        return (circleProgressBar.value * (to - from) + from).toFixed()
    }

    function setValue(value)
    {
        var to = circleSelector.to
        var from = circleSelector.from
        circleProgressBar.value = value / (to - from) - from
    }

    property int fontLabelSize: 10
    property string labelColor: "white"
    property int marginSize: 4

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: circleProgressBar.top

        anchors.bottomMargin: parent.marginSize

        font.family: robotoBold
        font.pixelSize: parent.fontLabelSize
        color: labelColor

        text: (parent.from).toFixed()
    }

    Text {
        anchors.left: circleProgressBar.right
        anchors.verticalCenter: parent.verticalCenter

        anchors.leftMargin: parent.marginSize

        font.family: robotoBold
        font.pixelSize: parent.fontLabelSize
        color: labelColor

        text: (parent.to / 4).toFixed()
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: circleProgressBar.bottom

        anchors.topMargin: parent.marginSize

        font.family: robotoBold
        font.pixelSize: parent.fontLabelSize
        color: labelColor

        text: (parent.to / 2).toFixed()
    }

    Text {
        anchors.right: circleProgressBar.left
        anchors.verticalCenter: parent.verticalCenter

        anchors.rightMargin: parent.marginSize

        font.family: robotoBold
        font.pixelSize: parent.fontLabelSize
        color: labelColor

        text: (parent.to * 3 / 4).toFixed()
    }

    // TODO animate state changes
    states: [
        State {
            name: "active"
            PropertyChanges {
                target: circleSelector
                opacity: 1
            }
            PropertyChanges {
                target: circleSelector
                anchors.leftMargin: circleSelector.leftMargin
            }
        },
        State {
            name: "inactive"
            PropertyChanges {
                target: circleSelector
                opacity: 0
            }
            PropertyChanges {
                target: circleSelector
                anchors.leftMargin: 500
            }
        }
    ]
}
