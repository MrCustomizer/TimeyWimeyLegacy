import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2

Rectangle {
    property alias fontPixelSize: time.font.pixelSize
    height: fontPixelSize
    color: "#00FFFFFF"

    Text {
        id: time
        color: "#ffffff"
        font.family: roboto
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.fill: parent

        text: Qt.formatTime(new Date(), "hh:mm")

        Timer {
            interval: 1000
            repeat: true
            running: true

            onTriggered: {
                time.text = Qt.formatTime(new Date(), "hh:mm")
            }
        }
    }
}
