import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3

Rectangle {
    id: navigationItem
    property string source: ""
    property string label: ""
    Layout.preferredHeight: icon.height + label.anchors.topMargin + label.font.pixelSize
    Layout.preferredWidth: 48
    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
    color: "#00FFFFFF"

    Image {
        id: icon
        source: parent.source
        height: 48
        width: 48
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
    }

    Text {
        id: label
        text: parent.label
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: icon.bottom
        anchors.topMargin: 3
        font.family: robotoBold
        font.bold: true
        font.pixelSize: 11
        color: view.yellow
    }
}
