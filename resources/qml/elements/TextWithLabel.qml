import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Rectangle {
    id: textWithLabel
    property alias labelText: label.text

    state: "visible"
    
    Text {
        id: content
        color: view.yellow
        anchors.left: parent.left
        anchors.top: parent.top
        font.family: robotoBold
        font.pixelSize: 13
        font.bold: true
    }
    Text {
        id: label
        color: view.grey
        anchors.left: parent.left
        anchors.top: parent.bottom
        anchors.topMargin: -2
        font.family: robotoBold
        font.pixelSize: 9
        font.bold: true
    }

    function textChanged(newText) {
        if (newText === "")
        {
            state = "invisible"
        } else {
            state = "visible"
        }
        content.text = newText
    }

    states: [
        State {
            name: "visible"
            PropertyChanges {
                target: textWithLabel
                Layout.preferredHeight: 16
            }
            PropertyChanges {
                target: textWithLabel
                opacity: 1
            }
        },
        State {
            name: "invisible"
            PropertyChanges {
                target: textWithLabel
                Layout.preferredHeight: 0
            }
            PropertyChanges {
                target: textWithLabel
                opacity: 0
            }
        }
    ]
}
