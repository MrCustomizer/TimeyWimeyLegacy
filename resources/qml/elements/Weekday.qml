import QtQuick 2.0

Rectangle {
    id: weekday
    width: 40
    height: 22
    color: "#ED387BBF"

    property alias name: title.text
    property var qtIndex

    Text {
        id: title
        color: "white"
        font.family: robotoBold
        font.bold: true
        font.pixelSize: 9
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked:
        {
            var enabled = parent.state === "active"
            if (enabled)
            {
                weekday.state = "inactive"
            }
            else {
                weekday.state = "active"
            }
            AlarmEditor.setWeekdayEnabled(qtIndex, !enabled)
        }
    }

    states: [
        State {
            name: "active"
            PropertyChanges {
                target: weekday
                opacity: 1
            }
        },
        State {
            name: "inactive"
            PropertyChanges {
                target: weekday
                opacity: 0.4
            }
        }
    ]
}
