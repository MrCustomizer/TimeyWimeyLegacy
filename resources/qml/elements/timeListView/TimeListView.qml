import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQml.Models 2.2

ListView {
    width: 30
    height: 85
    orientation: ListView.Vertical
    spacing: 10
    highlightRangeMode: ListView.StrictlyEnforceRange
    highlightMoveDuration: 200
    preferredHighlightBegin: height/2 - fontSize/2 - 1
    preferredHighlightEnd: height/2 - fontSize/2 - 1

    property int fontSize: 15
    
    delegate: TimeModelItem {
        id: delegate

        font.pixelSize: fontSize
    }
    
    function setValue(newValue) {
        var index = 0;

        for (var i = 0; i < model.rowCount(); i++) {
            var valueTarget = model.get(i).value
            if (newValue === valueTarget) {
                index = i
                break
            }
        }

        currentIndex = index
    }
}
