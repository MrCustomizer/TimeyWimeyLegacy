import QtQuick 2.0

Text {
    id: container
    font.bold: true
    font.family: robotoBold

    text: value
    color: ListView.isCurrentItem ? "white" : view.yellow;

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
            container.ListView.view.currentIndex = index
            container.forceActiveFocus()
        }
    }
}
