import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 320
    height: 240

    MainForm {
        transformOrigin: Item.Center
        width: 320
        height: 240
        anchors.fill: parent
        color: "#010b31"
    }
}
