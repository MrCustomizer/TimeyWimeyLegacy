import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Screen {
    Text {
        id: text
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 35
        anchors.top: parent.top
        color: view.yellow
        font.family: robotoBold
        font.pixelSize: 13
        font.bold: true

        text: "Starting playback. Please wait…"
    }
    ProgressBar {
        anchors.top: text.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: text.horizontalCenter

        value: 0
        minimumValue: 0
        maximumValue: 100
        style: ProgressBarStyle {
            background: Rectangle {
                radius: 2
                color: "#88ffffff"
                border.color: "gray"
                border.width: 1
                implicitWidth: 200
                implicitHeight: 12
            }
            progress: Rectangle {
                color: view.yellow
                border.color: "steelblue"
            }
        }

        Component.onCompleted: {
            MediaPlayer.onBufferStatusChanged.connect(updateProgress)
        }

        function updateProgress(percentFilled) {
            value = percentFilled
        }
    }
}
