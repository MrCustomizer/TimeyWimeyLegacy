import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2

import "qrc:/qml/elements"

Screen {
    property alias mouseArea: mouseArea

    Clock {
        id: time
        width: parent.width
        anchors.top: parent.top
        fontPixelSize: 100
    }

    Text {
        id: date

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: time.bottom
        anchors.topMargin: -4

        color: "#ffffff"
        font.family: robotoBold
        font.pixelSize: 10
        font.bold: true
        horizontalAlignment: Text.AlignHCenter

        text: Qt.formatDateTime(new Date(), "MM/dd/yy")

        Timer {
            interval: 1000
            repeat: true
            running: true

            onTriggered: {
                date.text = Qt.formatDateTime(new Date(), "MM/dd/yy")
            }
        }
    }


    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }
}
