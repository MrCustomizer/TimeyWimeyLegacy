import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

import "qrc:/qml/elements"

Screen {
    property alias mouseArea: mouseArea
    Image {
        id: albumArt

        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 3

        height: parent.height - 40
        width: height

        Component.onCompleted: {
            MediaPlayer.onNewAlbumArt.connect(changeAlbumArt)
        }

        function changeAlbumArt(albumArtUrl) {
            if (albumArtUrl !== "")
            {
                albumArt.source = albumArtUrl;
            } else {
                source = "";
            }
        }
    }

    ColumnLayout {
        anchors.left: albumArt.right
        anchors.leftMargin: 10
        spacing: 20

        anchors.verticalCenter: parent.verticalCenter

        TextWithLabel {
            id: radioStation
            labelText: qsTr("radio station")

            Component.onCompleted: {
                MediaPlayer.newRadioStation.connect(textChanged)
            }
        }
        TextWithLabel {
            id: artist
            labelText: qsTr("artist")

            Component.onCompleted: {
                MediaPlayer.newArtist.connect(textChanged)
            }
        }
        TextWithLabel {
            id: title
            labelText: qsTr("title")

            Component.onCompleted: {
                MediaPlayer.newTitle.connect(textChanged)
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }
}
