import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0

import "qrc:/qml/elements"
import "qrc:/qml/screens/alarmEditor"

RectangleWithBorders {
    id: screens

    anchors.left: parent.left
    anchors.top: parent.top
    width: parent.width
    color: "#FF000000"

    property alias blurBrightnessModifierColor: blurBrightnessModifier.color
    property alias blurTint: blurTint
    property alias homeScreen: homeScreen
    property alias snoozeScreen: snoozeScreen
    property alias alarmsScreen: alarmsScreen
    property alias alarmEditorScreen: alarmEditorScreen
    property alias playingScreen: playingScreen
    property alias alarmPlayingScreen: alarmPlayingScreen
    property alias bufferingScreen: bufferingScreen
    property alias slumberScreen: slumberScreen

    Rectangle {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 1
        width: parent.width
        height: parent.height - 2
        clip: true


        Blur {
            id: blur
        }

        BlurTint {
            id: blurTint
        }

        BrightnessModifier {
            id: blurBrightnessModifier
        }

        HomeScreen {
            id: homeScreen
        }

        SnoozeScreen {
            id: snoozeScreen
        }

        AlarmListScreen {
            id: alarmsScreen
        }

        AlarmEditorScreen {
            id: alarmEditorScreen
        }

        PlayingScreen {
            id: playingScreen
        }

        AlarmPlayingScreen {
            id: alarmPlayingScreen
        }

        BufferingScreen {
            id: bufferingScreen
        }

        SlumberScreen {
            id: slumberScreen
        }
    }
}
