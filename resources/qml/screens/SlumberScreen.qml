import QtQuick 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

import "qrc:/qml/elements"

Screen {
    property alias mouseArea: mouseArea
    // TODO unify this part with snoozescreen?
    Clock {
        id: time
        anchors.left: parent.left
        anchors.verticalCenter: circleSelector.verticalCenter
        anchors.leftMargin: 10
        width: parent.width - circleSelector.width

        fontPixelSize: 65
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    CircleSelector {
        id: circleSelector
        anchors.right: parent.right
        width: 120
        to: 20

        Component.onCompleted: MediaPlayer.onSlumberTimeUpdate.connect(changeSlumberTime)

        onValueChanged: {
            MediaPlayer.setSlumberTime(circleSelector.calcExternalValueFromInternalValue())
        }

        function changeSlumberTime(minutes) {
            setValue(minutes)
        }
    }

    ArtistAndTitleBanner {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: circleSelector.bottom
        anchors.topMargin: 10
    }
}
