import QtQuick 2.0

import "qrc:/qml/screens"
import "qrc:/qml/elements"

Screen {
    property alias newAlarmMouseArea: newAlarmMouseArea
    signal editAlarm

    Image {
        id: newAlarmButton
        source: "qrc:/graphics/newAlarmButton.png"

        MouseArea {
            id: newAlarmMouseArea
            anchors.fill: parent
        }
    }

    ListView {
        id: alarmsList
        anchors.left: parent.left
        anchors.leftMargin: 35
        anchors.topMargin: 27
        anchors.top: parent.top
        spacing: 2
        clip: true

        width: parent.width - anchors.leftMargin
        height: parent.height - anchors.topMargin

        model: AlarmsModel
        delegate: Rectangle {
            id: delegate
            color: "#00000000"
            height: 40
            width: parent.width
            state: alarmEnabled ? "active" : "inactive"

            Image {
                id: okButton
                source: "qrc:/graphics/okButton.png"
                height: 23
                width: 24
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        Settings.setAlarmEnabled(index, delegate.state === "inactive")
                    }
                }
            }

            Text {
                id: timeText
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 30
                color: view.yellow
                text: time
                font.family: robotoBold
                font.bold: true
                font.pixelSize: 18
            }
            Text {
                id: weekdays
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 58
                color: view.grey
                text: days
                font.family: robotoBold
                font.bold: true
                font.pixelSize: 10
            }
            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: 2
                anchors.left: timeText.left
                width: 208
                height: 33
                color: "#00FFFFFF"

                MouseArea {
                    id: editAlarmMouseArea
                    anchors.fill: parent

                    onClicked: {
                        alarmEditorScreen.editAlarmIndex = index
                        editAlarm()
                    }
                }
            }

            Image {
                id: deleteButton
                source: "qrc:/graphics/deleteButton.png"
                height: 24
                width: 24
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        Settings.deleteAlarm(index)
                    }
                }
            }
            Rectangle {
                id: border
                height: 1
                width: parent.width - 20
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                color: "#AAFFFFFF"

            }

            states: [
                State {
                    name: "active"
                    PropertyChanges {
                        target: delegate
                        opacity: 1
                    }
                },
                State {
                    name: "inactive"
                    PropertyChanges {
                        target: delegate
                        opacity: 0.5
                    }
                }
            ]
        }
    }
}
