/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef ALARM_H
#define ALARM_H

#include <QString>
#include <QSet>
#include <QList>
#include <QTime>
#include <QDateTime>
#include <QStringList>

class Alarm
{
public:
    Alarm();

    Alarm(QString hours, QString minutes, QStringList weekdays, bool enabled);

    QSet<Qt::DayOfWeek> getWeekdays();

    QTime getTime();

    QList<QDateTime> getAlarmDateTimes(QDateTime reference) const;

    void setHours(QString hours);

    void setMinutes(QString minutes);

    void setWeekday(int day, bool enabled);

    bool isEnabled();

    void setEnabled(bool enabled);

private:
    QSet<Qt::DayOfWeek> weekdays;
    QTime time;
    bool enabled = true;

    void setWeekday(Qt::DayOfWeek day, bool enabled);
};

#endif // ALARM_H
