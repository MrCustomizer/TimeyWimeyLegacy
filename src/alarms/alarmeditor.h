/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef ALARMEDITOR_H
#define ALARMEDITOR_H

#include "alarm.h"
#include "../settingsmanager.h"

#include <QObject>

class AlarmEditor : public QObject
{
    Q_OBJECT
public:
    explicit AlarmEditor(SettingsManager *settings, QObject *parent = 0);

    Q_INVOKABLE void createNewAlarm();

    Q_INVOKABLE void startEditing(int alarmNr);

    Q_INVOKABLE void confirmAlarm();

    Q_INVOKABLE void abortAlarmEditing();

    Q_INVOKABLE bool isEditing();

    Q_INVOKABLE QString getHours();

    Q_INVOKABLE QString getMinutes();

    Q_INVOKABLE bool isWeekdayEnabled(int weekday);

    Q_INVOKABLE void setWeekdayEnabled(int weekday, bool enabled);

    int getAlarmNr() const;

    Alarm getAlarm() const;

public slots:
    void hoursChanged(QString hours);
    void minutesChanged(QString minutes);

private:
    SettingsManager *settings;
    Alarm alarm;
    int alarmNr = -1;
    bool editing = false;
};

#endif // ALARMEDITOR_H
