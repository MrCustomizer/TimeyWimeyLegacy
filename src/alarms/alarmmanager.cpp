/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "alarmmanager.h"

AlarmManager::AlarmManager(SettingsManager *settings,
                           MediaPlayer *player,
                           AlarmEditor *editor,
                           QObject *parent) : QObject(parent)
{
    this->settings = settings;
    this->player = player;
    this->editor = editor;

    reference = QDateTime::currentDateTime();

    connect(&alarmCheckTimer, &QTimer::timeout, this, &AlarmManager::alarmCheckTimeout);
    alarmCheckTimer.start(1000);

    snoozeTimer.setInterval(1000 * 60);
    connect(&snoozeTimer, &QTimer::timeout, this, &AlarmManager::snoozeCheck);
}

QString AlarmManager::getNextAlarmString()
{
    QString alarmString("");

    QDateTime alarm = getNextAlarm(true);
    if (alarm.isValid()) {
        return createAlarmString(alarm);
    }

    return alarmString;
}

QString AlarmManager::getTimeToNextAlarmString()
{
    QString timeTo("");

    QDateTime alarm = getNextAlarm(true);
    if (alarm.isValid()) {
        return createTimeToAlarmString(alarm);
    }

    return timeTo;
}

void AlarmManager::stopAlarm()
{
    snoozeMinutes = -1;
    player->stop();
}

void AlarmManager::snooze()
{
    player->stop();
    snoozeMinutes = 8;
    emit snoozeTimeUpdate(snoozeMinutes);
    snoozeTimer.start();
}

void AlarmManager::setSnoozeTime(QString minutes)
{
    snoozeMinutes = minutes.toInt();
}

void AlarmManager::stopSnoozing()
{
    snoozeTimer.stop();
}

void AlarmManager::alarmCheckTimeout()
{
    QDateTime current = QDateTime::currentDateTime();

    QDateTime alarm = getNextAlarm(false);
    if (alarm.isValid() && alarm < current) {
        reference = current;
        startAlarm();
    }
}

void AlarmManager::snoozeCheck()
{
    snoozeMinutes--;
    if (snoozeMinutes <= 0) {
        snoozeTimer.stop();
        startAlarm();
    }
    emit snoozeTimeUpdate(snoozeMinutes);
}

QDateTime AlarmManager::getNextAlarm(bool considerEditedAlarm)
{
    QList<QDateTime> potentialAlarms = getPotentialAlarms(considerEditedAlarm);

    std::sort(potentialAlarms.begin(), potentialAlarms.end());

    if (potentialAlarms.size() > 0) {
        return potentialAlarms.at(0);
    }

    return QDateTime();
}

QList<QDateTime> AlarmManager::getPotentialAlarms(bool considerEditedAlarm)
{
    QList<Alarm> alarms;

    if (considerEditedAlarm && editor->isEditing()) {
        alarms = settings->getAlarmsExceptOne(editor->getAlarmNr());
        alarms << editor->getAlarm();
    } else {
        alarms = settings->getAlarms();
    }
    QList<QDateTime> potentialAlarms;

    for (Alarm alarm : alarms) {
        if (alarm.isEnabled()) {
            if (considerEditedAlarm) {
                potentialAlarms.append(alarm.getAlarmDateTimes(QDateTime::currentDateTime()));
            } else {
                potentialAlarms.append(alarm.getAlarmDateTimes(reference));
            }
        }
    }

    return potentialAlarms;
}

QString AlarmManager::createAlarmString(QDateTime alarmIntern)
{
    QString alarmString;

    alarmString += alarmIntern.date().toString("dd.MM") + " ";
    alarmString += alarmIntern.time().toString("hh:mm");

    return alarmString;
}

QString AlarmManager::createTimeToAlarmString(QDateTime alarmIntern)
{
    QString timeTo;
    QDateTime current = QDateTime::currentDateTime();
    qint64 offset = current.msecsTo(alarmIntern);
    offset += 60000;

    qint64 days = offset / (1000 * 60 * 60 * 24);
    offset -= days * 24 * 60 * 60 * 1000;
    qint64 hours = offset / 1000 / 60 / 60;
    offset -= hours * 60 * 60 * 1000;
    qint64 minutes = offset / 1000 / 60;

    timeTo = QString::number(days) + "d ";
    timeTo += QString::number(hours) + "h ";
    timeTo += QString::number(minutes) + "m";

    return timeTo;
}

void AlarmManager::startAlarm()
{
    player->startAlarm();
    emit alarmTriggered();
}
