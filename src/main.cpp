/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtGlobal>
#include <QString>
#include <QByteArray>
#include <QDebug>
#include <QCommandLineOption>
#include <QCommandLineParser>

#include <cstdio>
#include <syslog.h>
#include "systemhelpers.h"
#include "settingsmanager.h"
#include "alarms/alarmlistmodel.h"
#include "alarms/alarmmanager.h"
#include "alarms/alarmeditor.h"
#include "sensors/backlightdimmer.h"
#include "sensors/lightsensortracker.h"
#include "sensors/pitftbuttonhandler.h"


// Handler for Qt log messages that sends output to syslog as well as standard error.
void SyslogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)

    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", localMsg.constData());
        syslog(LOG_DEBUG, "debug: %s", localMsg.constData());
        break;
    case QtInfoMsg:
        fprintf(stderr, "Info: %s\n", localMsg.constData());
        syslog(LOG_INFO, "info: %s", localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", localMsg.constData());
        syslog(LOG_WARNING, "warning: %s", localMsg.constData());
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", localMsg.constData());
        syslog(LOG_CRIT, "critical: %s", localMsg.constData());
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", localMsg.constData());
        syslog(LOG_ALERT, "alert: %s", localMsg.constData());
        abort();
    }
}


int main(int argc, char *argv[])
{
    qInstallMessageHandler(SyslogMessageHandler);

    qInfo("Starting TimeyWimey");

    QGuiApplication app(argc, argv);

    QCommandLineOption optTestFile({"t", "testAudio"}, "Shorter version of fallback audio for tests");

    QCommandLineParser parser;
    parser.addOption(optTestFile);
    parser.process ( QCoreApplication::arguments());
    bool testFile = parser.isSet(optTestFile);

    QQmlApplicationEngine engine;

    app.setOrganizationName("Localtoast Software");
    app.setApplicationName("Timey Wimey");

    SystemHelpers systemHelpers;
    engine.rootContext()->setContextProperty("SystemHelpers", &systemHelpers);

    MediaPlayer mediaPlayer(testFile);
    engine.rootContext()->setContextProperty("MediaPlayer", &mediaPlayer);

    SettingsManager settings;
    engine.rootContext()->setContextProperty("Settings", &settings);

    AlarmListModel alarmsModel(&settings);
    engine.rootContext()->setContextProperty("AlarmsModel", &alarmsModel);

    AlarmEditor alarmEditor(&settings);
    engine.rootContext()->setContextProperty("AlarmEditor", &alarmEditor);

    AlarmManager alarmManager(&settings, &mediaPlayer, &alarmEditor);
    engine.rootContext()->setContextProperty("AlarmManager", &alarmManager);

    LightSensorTracker lightSensorTracker(0);
    engine.rootContext()->setContextProperty("LightSensorTracker", &lightSensorTracker);

    if (systemHelpers.isRunningOnPi()) {
        QString gpio = systemHelpers.getGpioExecutableWithPath();
        BacklightDimmer dimmer(&lightSensorTracker, gpio);
        PiTftButtonHandler buttonHandler(gpio);
        engine.rootContext()->setContextProperty("PiTftButtonHandler", &buttonHandler);

        engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
        return app.exec();
    }

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));



    return app.exec();
}
