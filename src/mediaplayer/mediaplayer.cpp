/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "mediaplayer.h"

MediaPlayer::MediaPlayer(bool testFile, QObject *parent) : QObject(parent),
    player(parent, QMediaPlayer::StreamPlayback),
    stateMachine(parent)
{
    connect(&player, &QMediaPlayer::mediaStatusChanged, this, &MediaPlayer::mediaStatusChanged);
    connect(&player, &QMediaPlayer::bufferStatusChanged, this, &MediaPlayer::bufferStatusChanged);
    connect(&player, QOverload<const QString &, const QVariant &>::of(&QMediaPlayer::metaDataChanged),
            this, &MediaPlayer::metaDataChanged);
    connect(&player, QOverload<QMediaPlayer::Error>::of(&QMediaPlayer::error), this,
            &MediaPlayer::error);

    this->testFile = testFile;
    // DO i still need this?
    connect(&volume, &VolumeControl::volumeChanged, this, &MediaPlayer::volumeChanged);

    slumberTimer.setInterval(60 * 1000);
    connect(&slumberTimer, &QTimer::timeout, this, &MediaPlayer::checkSlumberState);

    initStateMachine();
}

void MediaPlayer::start()
{
    startIntern(800);
}

void MediaPlayer::stop()
{
    stop(800);
}

void MediaPlayer::startSlumberMode()
{
    if (!slumberTimer.isActive()) {
        start();
        slumberMinutes = 10;
        emit slumberTimeUpdate(slumberMinutes);
        slumberTimer.start();
    }
}

void MediaPlayer::setSlumberTime(QString minutes)
{
    slumberMinutes = minutes.toInt();
}

bool MediaPlayer::isPlaying()
{
    return player.state() == QMediaPlayer::State::PlayingState;
}

void MediaPlayer::setVolume(int volume)
{
    player.setVolume(volume);
}

void MediaPlayer::startAlarm()
{
    startIntern(30000);
}

void MediaPlayer::mediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    switch (status) {
    case QMediaPlayer::MediaStatus::StalledMedia:
        bufferingInProgress = QDateTime::currentDateTime();
        break;
    case QMediaPlayer::MediaStatus::BufferingMedia:
        bufferingInProgress = QDateTime::currentDateTime();
        break;
    case QMediaPlayer::MediaStatus::LoadedMedia:
        player.play();
        emit finishedBuffering();
        break;
    default:
        break;
    }
}

void MediaPlayer::metaDataChanged(const QString &key, const QVariant &value)
{
    QUrl fallbackUrl = QUrl("qrc:/music/backupSong.mp3");
    QUrl currentUrl = player.currentMedia().canonicalUrl();
    if (key == "Title") {
        if (currentUrl == fallbackUrl) {
            emit newTitle(value.toString());
        } else {
            updateArtistAndTitle(value.toString());
        }
    } else if (key == "AlbumArtist") {
        if (currentUrl == fallbackUrl) {
            emit newArtist(value.toString());
        }
    } else if (key == "homepage") {
        emit newAlbumArt(value.toString());
    } else if (key == "location") {
        emit newRadioStation(value.toString());
    }
}

void MediaPlayer::error(QMediaPlayer::Error error)
{
    qDebug() << "Error playing stream, switching to fallback song: " << error;
    playFallbackSong();
}

void MediaPlayer::volumeChanged(int volume)
{
    player.setVolume(volume);
}

void MediaPlayer::checkSlumberState()
{
    slumberMinutes--;
    emit slumberTimeUpdate(slumberMinutes);
    if (slumberMinutes == 0) {
        stop(30000);
    }
}

void MediaPlayer::fadingStateEntered()
{
    volume.setFadingDuration(nextFadingDuration);
    if (nextFadingDirection == VolumeControl::FADING_DIRECTION::FADING_IN) {
        volume.startFadingIn();
    } else {
        volume.startFadingOut();
    }
}

void MediaPlayer::initStateMachine()
{
    QState *stoppedState = new QState();
    QState *bufferingState = new QState();
    QState *fadingState = new QState();
    QState *playingState = new QState();

    // Setup stoppedState
    stoppedState->addTransition(this, &MediaPlayer::startedBuffering, bufferingState);

    // Setup bufferingState
    bufferingState->addTransition(this, &MediaPlayer::finishedBuffering, fadingState);
    QSignalTransition *bufferingFadingInTrans = new QSignalTransition(this,
                                                                      &MediaPlayer::finishedBuffering);
    connect(bufferingFadingInTrans, &QSignalTransition::triggered, this,
            &MediaPlayer::fadingStateEntered);
    bufferingState->addTransition(bufferingFadingInTrans);

    // Setup fadingState
    connect(fadingState, &QState::entered, this, &MediaPlayer::fadingStateEntered);
    fadingState->addTransition(&volume, &VolumeControl::fadeInFinished, playingState);
    fadingState->addTransition(this, &MediaPlayer::stopped, stoppedState);

    QSignalTransition *fadingOutTrans = new QSignalTransition(this, &MediaPlayer::stopping);
    QSignalTransition *fadingInTrans = new QSignalTransition(this, &MediaPlayer::finishedBuffering);
    fadingState->addTransition(fadingOutTrans);
    fadingState->addTransition(fadingInTrans);

    connect(fadingOutTrans, &QSignalTransition::triggered, this, &MediaPlayer::fadingStateEntered);
    connect(fadingInTrans, &QSignalTransition::triggered, this, &MediaPlayer::fadingStateEntered);

    QSignalTransition *fadeOutFinishedTrans = new QSignalTransition(&volume,
                                                                    &VolumeControl::fadeOutFinished);
    fadingState->addTransition(fadeOutFinishedTrans);
    connect(fadeOutFinishedTrans, &QSignalTransition::triggered, this, &MediaPlayer::stopIntern);

    // Setup playingState
    playingState->addTransition(this, &MediaPlayer::stopping, fadingState);

    // Setup stateMachine
    stateMachine.addState(stoppedState);
    stateMachine.addState(bufferingState);
    stateMachine.addState(playingState);
    stateMachine.addState(fadingState);

    stateMachine.setInitialState(stoppedState);
    stateMachine.start();
}

void MediaPlayer::startIntern(int fadingDuration)
{
    nextFadingDuration = fadingDuration;
    nextFadingDirection = VolumeControl::FADING_DIRECTION::FADING_IN;
    if (player.state() != QMediaPlayer::State::PlayingState) {
        emit startedBuffering();
        volumeChanged(0);
        player.setMedia(QMediaContent(QString("http://stream-uk1.radioparadise.com/aac-128")));
    } else {
        emit finishedBuffering();
    }
}

void MediaPlayer::stop(int fadingDuration)
{
    nextFadingDuration = fadingDuration;
    nextFadingDirection = VolumeControl::FADING_DIRECTION::FADING_OUT;
    slumberTimer.stop();
    emit stopping();
}

void MediaPlayer::stopIntern()
{
    emit newArtist("");
    emit newTitle("");
    emit newAlbumArt("");

    player.stop();
    player.setMedia(QMediaContent());
    emit stopped();
}

void MediaPlayer::playFallbackSong()
{
    volumeChanged(0);
    player.stop();

    QMediaPlaylist *playlist = new QMediaPlaylist();
    playlist->setPlaybackMode(QMediaPlaylist::Loop);
    if (testFile) {
        playlist->addMedia(QUrl("qrc:/music/backupSongShort.mp3"));
    } else {
        playlist->addMedia(QUrl("qrc:/music/backupSong.mp3"));
    }
    player.setPlaylist(playlist);
    volume.setFadingDuration(800);
    volume.startFadingIn();
    emit newRadioStation("");
    emit newAlbumArt("");
}

void MediaPlayer::updateArtistAndTitle(QString artistAndTitle)
{
    if (!artistAndTitle.isEmpty()) {
        QStringList split = artistAndTitle.split(" - ");
        if (split.length() == 2) {
            emit newArtist(split.at(0));
            emit newTitle(split.at(1));
        }
    }
}
