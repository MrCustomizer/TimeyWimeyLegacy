/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include "volumecontrol.h"

#include <QObject>
#include <QtMultimedia/QMediaPlayer>
#include <QMediaStreamsControl>
#include <QMediaService>
#include <QEventLoop>
#include <QNetworkReply>
#include <QSignalTransition>
#include <QMediaPlaylist>
#include <QMediaContent>
#include <QStateMachine>
#include <QtGlobal>

#include <memory>

class MediaPlayer : public QObject
{
    Q_OBJECT
public:
    MediaPlayer(bool testFile, QObject *parent = 0);

    Q_INVOKABLE void start();

    Q_INVOKABLE void stop();

    Q_INVOKABLE void startSlumberMode();

    Q_INVOKABLE void setSlumberTime(QString minutes);

    Q_INVOKABLE bool isPlaying();

    void setVolume(int volume);

    void startAlarm();

signals:
    void newArtist(QString newArtist);
    void newTitle(QString newTitle);
    void newAlbumArt(QString newAlbumArt);
    void newRadioStation(QString radioStation);

    void finishedBuffering();
    void stopping();
    void stopped();
    void startedBuffering();

    void slumberTimeUpdate(int slumberTime);
    void bufferStatusChanged(int percentFilled);

public slots:
    void volumeChanged(int volume);

private slots:
    void checkSlumberState();

    void fadingStateEntered();
    void mediaStatusChanged(QMediaPlayer::MediaStatus status);
    void metaDataChanged(const QString &key, const QVariant &value);
    void error(QMediaPlayer::Error error);

private:
    QMediaPlayer player;
    VolumeControl volume;
    QStateMachine stateMachine;
    QTimer slumberTimer;
    int slumberMinutes = 0;
    qint32 nextFadingDuration = 0;
    VolumeControl::FADING_DIRECTION nextFadingDirection;
    bool testFile;
    QDateTime bufferingInProgress;

    void initStateMachine();
    void startIntern(int fadingDuration);
    void stop(int fadingDuration);
    void stopIntern();
    void startStreaming();
    void stopStreaming();
    void playFallbackSong();
    void updateArtistAndTitle(QString artistAndTitle);
};

#endif // MEDIAPLAYER_H
