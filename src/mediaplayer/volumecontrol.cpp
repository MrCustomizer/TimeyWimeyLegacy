/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "volumecontrol.h"

VolumeControl::VolumeControl(QObject *parent) : QObject(parent)
{
}

void VolumeControl::setFadingDuration(int msecs)
{
    duration = msecs;
}

void VolumeControl::startFadingIn()
{
    state = FADING_IN;
    fadeIn();
}

void VolumeControl::startFadingOut()
{
    state = FADING_OUT;
    fadeOut();
}

void VolumeControl::fadeIn()
{
    if (state == FADING_IN) {
        emit volumeChanged(currentVolume);

        int interval = duration / maxVolume;
        currentVolume++;
        if (currentVolume <= maxVolume) {
            // TODO make all singleshots to member timers (no addition on concurrent calls)
            QTimer::singleShot(interval, this, SLOT(fadeIn()));
        } else {
            state = UNDEFINED;
            emit fadeInFinished();
        }
    }
}

void VolumeControl::fadeOut()
{
    if (state == FADING_OUT) {
        emit volumeChanged(currentVolume);

        currentVolume--;
        int interval = duration / maxVolume;
        if (currentVolume > 0) {
            QTimer::singleShot(interval, this, SLOT(fadeOut()));
        } else {
            state = UNDEFINED;
            emit fadeOutFinished();
        }
    }
}
