/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "backlightdimmer.h"

BacklightDimmer::BacklightDimmer(LightSensorTracker *lightTracker,
                                 QString gpio,
                                 QObject *parent) : QObject(parent), animation(this, "pwmProperty")
{
    this->gpio = gpio;
    setupGpio();
    setPwm(1024);
    connect(lightTracker, &LightSensorTracker::sensorUpdatedInt, this, &BacklightDimmer::luxChanged);
}

void BacklightDimmer::luxChanged(int lux)
{
    int luxMax = 30;
    int luxMin = 0;

    lux = std::min(lux, luxMax);
    lux = std::max(lux, luxMin);

    qint32 targetPwm = (double) lux / 30 * (double) 1024;
    qint32 minPwm = 3;
    targetPwm = std::max(minPwm, targetPwm);

    if (targetPwm != pwm) {
        animation.setDuration(400);
        animation.setEndValue(targetPwm);
        animation.start();
    }
}

void BacklightDimmer::setPwm(int pwm)
{
    this->pwm = pwm;
    QStringList arguments;
    arguments << "-g" << "pwm" << "18" << QString::number(pwm);
    QProcess::startDetached(gpio, arguments);
}

void BacklightDimmer::setupGpio()
{
    QStringList arguments;
    arguments << "-g" << "mode" << "18" << "pwm";
    QProcess::startDetached(gpio, arguments);
}
