/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef BACKLIGHTDIMMER_H
#define BACKLIGHTDIMMER_H

#include "lightsensortracker.h"

#include <QObject>
#include <QProcess>
#include <QPropertyAnimation>
#include <algorithm>

class BacklightDimmer : public QObject
{
    Q_OBJECT
public:
    explicit BacklightDimmer(LightSensorTracker *lightTracker, QString gpio, QObject *parent = 0);

signals:
    void pwmChanged();

private slots:
    void luxChanged(int lux);

private:
    Q_PROPERTY(int pwmProperty MEMBER pwm WRITE setPwm NOTIFY pwmChanged);
    int pwm = 1024;
    QPropertyAnimation animation;
    QString gpio;

    void setupGpio();
    void setPwm(int pwm);
};

#endif // BACKLIGHTDIMMER_H
