/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "lightsensortracker.h"

LightSensorTracker::LightSensorTracker(QObject *parent) : QObject(parent)
{
    i2cDevice = tsl2561_init(i2cAddress, i2cDevicePath.toStdString().c_str());

    if (i2cDevice != Q_NULLPTR) {
        tsl2561_enable_autogain(i2cDevice);
        tsl2561_set_integration_time(i2cDevice, TSL2561_INTEGRATION_TIME_13MS);
    }
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(runUpdate()));
    timer->start(600);
}

LightSensorTracker::~LightSensorTracker()
{
    if (i2cDevice != Q_NULLPTR) {
        tsl2561_close(i2cDevice);
    }
    delete timer;
}

void LightSensorTracker::runUpdate()
{
    int lux = getLux();
    emit sensorUpdatedInt(lux);
    emit sensorUpdated(QString::number(lux));
}

int LightSensorTracker::getLux()
{
    /*
     * If we weren't able to initialize a tsl2561 device, we will just return a lux value, which
     * is always big enough, so that the display never get's dimmed.
     */
    if (i2cDevice == Q_NULLPTR) {
        return 8000;
    }
    int lux = tsl2561_lux(i2cDevice);
    return lux;
}
