/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef LIGHTSENSORTRACKER_H
#define LIGHTSENSORTRACKER_H

#include <QObject>
#include <QTimer>

#include <libs/tsl2561/src/tsl2561.h>



class LightSensorTracker: public QObject
{
    Q_OBJECT
public:
    explicit LightSensorTracker(QObject *parent = 0);
    ~LightSensorTracker();

signals:
    void sensorUpdated(QString lux);
    void sensorUpdatedInt(int lux);

private slots:
    void runUpdate();

private:
    int i2cAddress = 0x39;
    QString i2cDevicePath = "/dev/i2c-1";
    void *i2cDevice;
    QTimer *timer;

    int getLux();
};

#endif // LIGHTSENSORTRACKER_H
