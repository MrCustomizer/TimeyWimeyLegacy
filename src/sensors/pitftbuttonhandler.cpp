/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "pitftbuttonhandler.h"

WiringPiTrigger wiringpiTrigger;

void button1InterruptHandler()
{
    emit wiringpiTrigger.buttonPressed(1);
}

void button2InterruptHandler()
{
    emit wiringpiTrigger.buttonPressed(2);
}

void button3InterruptHandler()
{
    emit wiringpiTrigger.buttonPressed(3);
}

void button4InterruptHandler()
{
    emit wiringpiTrigger.buttonPressed(4);
}


PiTftButtonHandler::PiTftButtonHandler(QString gpio, QObject *parent) : QObject(parent)
{
    this->gpio = gpio;
    connect(&wiringpiTrigger, &WiringPiTrigger::buttonPressed, this,
            &PiTftButtonHandler::buttonPressedInternal);

    exportPin(17);
    exportPin(22);
    exportPin(23);
    exportPin(27);

    setPulldownResistor(17);
    setPulldownResistor(22);
    setPulldownResistor(23);
    setPulldownResistor(27);

    wiringPiSetupSys();
    wiringPiISR(17, INT_EDGE_FALLING, &button1InterruptHandler);
    wiringPiISR(22, INT_EDGE_FALLING, &button2InterruptHandler);
    wiringPiISR(23, INT_EDGE_FALLING, &button3InterruptHandler);
    wiringPiISR(27, INT_EDGE_FALLING, &button4InterruptHandler);
}

void PiTftButtonHandler::buttonPressedInternal(int nr)
{
    if (!lastPress.contains(nr)) {
        emit buttonPressed(nr);
    } else if (lastPress[nr].msecsTo(QDateTime::currentDateTime()) > 100) {
        emit buttonPressed(nr);
    }

    lastPress[nr] = QDateTime::currentDateTime();
}

void PiTftButtonHandler::exportPin(int pin)
{
    QStringList arguments;
    arguments << "export" << QString::number(pin) << "in";
    QProcess::startDetached(gpio, arguments);
}

void PiTftButtonHandler::setPulldownResistor(int pin)
{
    QStringList arguments;
    arguments << "-g" << "mode" << QString::number(pin) << "up";
    QProcess::startDetached(gpio, arguments);
}

