/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef PITFTBUTTONHANDLER_H
#define PITFTBUTTONHANDLER_H

#include <QObject>
#include <QProcess>
#include <QDateTime>
#include <QHash>

#include "wiringPi.h"

#include "wiringpitrigger.h"

class PiTftButtonHandler : public QObject
{
    Q_OBJECT
public:
    explicit PiTftButtonHandler(QString gpio, QObject *parent = 0);

signals:
    void buttonPressed(int nr);

private slots:
    void buttonPressedInternal(int nr);

private:
    void exportPin(int pin);
    void setPulldownResistor(int pin);
    QHash<int, QDateTime> lastPress;
    QString gpio;
};

#endif // PITFTBUTTONHANDLER_H
