/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "systemhelpers.h"

SystemHelpers::SystemHelpers()
{

}

bool SystemHelpers::isRunningOnPi()
{
    return getSystem() == Machine::RaspberryPi;
}


QUrl SystemHelpers::getAbsoluteFilePath(QUrl url)
{
    QString path = url.path();
    if (url.scheme() != "") {
        url.setScheme("");
        path = url.path();
        path = path.right(path.length() - 1);
    }
    return QUrl::fromLocalFile(QFileInfo(path).absoluteFilePath());
}

void SystemHelpers::shutdown()
{
    callShutdownCommand("-h");
}

void SystemHelpers::reboot()
{
    callShutdownCommand("-r");
}

QString SystemHelpers::getFont()
{
    return "Roboto";
}

QString SystemHelpers::getBoldFont()
{
    return "Roboto Bold";
}

QString SystemHelpers::getGpioExecutableWithPath()
{
    QString gpio = "gpio";
    QFileInfo gpioFile = QFileInfo("/usr/bin/" + gpio);
    if (gpioFile.exists()) {
        return gpioFile.absoluteFilePath();
    }
    gpioFile = QFileInfo("/usr/local/bin/" + gpio);
    if (gpioFile.exists()) {
        return gpioFile.absoluteFilePath();
    }
}

SystemHelpers::Machine SystemHelpers::getSystem()
{
    if (QSysInfo::currentCpuArchitecture() == "arm") {
        return Machine::RaspberryPi;
    }

    return Machine::Other;
}

void SystemHelpers::callShutdownCommand(QString parameter)
{
    /*
     * I don't want to risk shutting down my development machine, so I only execute this command
     * on the Raspberry Pi.
     */
    if (isRunningOnPi()) {
        QStringList arguments;
        arguments << "/sbin/shutdown" << parameter << "now";
        QProcess::startDetached("/usr/bin/sudo", arguments);
    }
}
