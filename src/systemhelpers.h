/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef SYSTEMHELPERS_H
#define SYSTEMHELPERS_H

#include <QObject>
#include <QSysInfo>
#include <QUrl>
#include <QFileInfo>
#include <QProcess>

class SystemHelpers : public QObject
{
    Q_OBJECT
public:
    SystemHelpers();

    enum Machine {
        RaspberryPi,
        Other
    };

    Q_INVOKABLE bool isRunningOnPi();

    Q_INVOKABLE QUrl getAbsoluteFilePath(QUrl path);

    Q_INVOKABLE void shutdown();

    Q_INVOKABLE void reboot();

    Q_INVOKABLE QString getFont();

    Q_INVOKABLE QString getBoldFont();

    QString getGpioExecutableWithPath();

private:
    Machine getSystem();

    void callShutdownCommand(QString parameter);
};

#endif // SYSTEMHELPERS_H
